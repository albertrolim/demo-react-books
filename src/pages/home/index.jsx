import { useState } from "react";
import booksObject from "../../books.json";
import styled from "styled-components";
import Header from "../../componetes/Header";
import "./index.scss";

export const Status = {
  List: "list",
  Grade: "grade",
};

const Home = () => {
  const [books] = useState(booksObject.books);
  const [listStatus, setListStatus] = useState(Status.List);

  console.log(listStatus)

  return (
    <>
      <Header setListStatus={setListStatus} listStatus={listStatus} />
      <div className="list">
        {books.map((item, i) => {
          if (listStatus == Status.List) {
            return (
              <div className="section" key={item.nome + i}>
                  <img className="image-size" src={item.capa} alt="new"></img>
                    <div className="section-list"> 
                      <li className="name">{item.nome}</li>
                        <li className="nome-autor">{item.nomeDoAutor}</li>
                          <div className="content">
                            <li className="serie">{item.serie} -</li>
                            <li className="avaliacao">{item.avaliacao} - </li>
                            <li className="progresso">{item.progresssoDoUsuario}</li>
                            <li className="ultima-leitura">{item.dataDaUltimaLeitura}</li>
                          </div>
                    </div>
              </div>

            );
          }
        }
        )}
      </div>

      <div  className="grade">
        {books.map((item, i) => {
          if (listStatus == Status.Grade) {
            return (
              <div  className="section" key={item.nome + i}>
                <p>
                  {" "}
                  <img className="image-size" src={item.capa} alt="new"></img>
                </p>
                  <div className="section-list"> 
                    <li className="name">{item.nome}</li>
                    <li className="nome-autor">{item.nomeDoAutor}</li>
                    <li className="serie">{item.serie}</li>
                    <li className="resumo-livro">{item.resumoDoLivro}</li>
                    <li className="avaliacao">{item.avaliacao}</li>
                  </div>
              </div>

            );
          }
        }
        )}
      </div>



    </>
  );
};
export default Home;
