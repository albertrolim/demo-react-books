import { useEffect, useState } from "react";
import "./index.css";
import { Status } from '../../pages/home'

const ControlListDisplay = ({ setListStatus, listStatus }) => {


  return (
    <>
      <div>
        <label className="switch">
          <input type="checkbox" onChange={(e) => {
            setListStatus(listStatus == Status.List ? Status.Grade : Status.List)
          }
          } />
          <span className="slider round"></span>
        </label>
      </div>
    </>
  );
};

export default ControlListDisplay;
