import ViewListButton from "../ViewListButton";

function Header({ setListStatus, listStatus}) {
  return <ViewListButton setListStatus={setListStatus} listStatus={listStatus} />;
}
export default Header;
