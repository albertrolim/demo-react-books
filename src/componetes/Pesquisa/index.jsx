import Input from "../Input";
import styled from "styled-components";

const PesquisarContainer = styled.section`
  color: #fff;
  text-align: center;
  padding: 85px 0;
  height: 270px;
  width: 100%;
`;
const Titulo = styled.h2`
  color: #fff;
  font-size: 40px;
  text-align: center;
  width: 100%;
`;
const Subtitulo = styled.h3`
  font-size: 20px;
  font-weight: 500;d
  margin-bottom: 40px;
`;
function Pesquisa() {
  return (
    <PesquisarContainer>
      <Titulo> Biblioteca </Titulo>
      <Subtitulo> Pesquisar na Biblioteca </Subtitulo>
      <Input placeholder="Livro.." />
    </PesquisarContainer>
  );
}

export default Pesquisa;
